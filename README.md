# Hello World
This exercise is used to give students an example of how submissions to
LM Codequest Academy work. 

## Useful Links
- [LM Codequest Academy Problem: Hello
World](https://lmcodequestacademy.com/problem/hello-world)

## Tips
- `sys.stdin.readline()` is nearly identical to `input()`, and when you're
  writing your own code `input()` is generally preferable. Since we're using
  Codequest's autograder for these problems, however, stick to
  `sys.stdin.readline()` to avoid weird edge case issues.

## Legal
All exercises are property of LM Codequest Academy and are used here purely for educational purposes. 
